<?php
/* @var $this UserController */
/* @var $data User */
?>

<div class="col-md-3">
	<a href="https://www.facebook.com/<?php echo $data->username;  ?>">
	    <?php
	        $headers = get_headers('http://graph.facebook.com/'.$data->username.'/picture?width=200&height=200',1);
	        echo CHtml::image($headers['Location'],"", $htmlOptions=array('class'=>'img-thumbnail') );
	    ?>
    </a>
</div>