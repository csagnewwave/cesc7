<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List User', 'url'=>array('index')),
	array('label'=>'Create User', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#user-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Users</h1>

<?php
echo CHtml::link("<b>จำนวนน้องที่ส่งเอกสารเรียบร้อยแล้ว (คลิกเพื่อดู)</b><br/>",array('user/list'),array('target'=>'_blank'));
?>
Robot :
<?php
	$count = User::Model()->count("filestatus=1 AND camp='Robot'");
	echo $count." คน<br/>";
?>
Network :
<?php

	$count = User::Model()->count("filestatus=1 AND camp='Network'");
	echo $count." คน<br/>";
?>

<br/>
<b>จำนวนน้องที่รอการตรวจใบสมัคร :</b>
<?php
	$sql = "SELECT COUNT(*) FROM cesc_user WHERE nickname <> '' AND sheet_gen3 <> '' AND filestatus=0";
	$count = Yii::app()->db->createCommand($sql)->queryScalar();
	echo $count." คน<br/>";
?>

<?php
	$taProvider=new CActiveDataProvider('User',array(
                'criteria'=>array(
				        'condition'=>"nickname <> '' AND sheet_gen3 <> '' AND filestatus=0",
				),  
            ));
	$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$taProvider,
	'itemView'=>'_list',
	));
?>
<hr/>
<?php
	echo(CHtml::beginForm());	
	echo(CHtml::label('ชื่อที่ต้องการค้น', 'name'));
	echo CHtml::textField('FORMNAME[fname]');
	echo(CHtml::submitButton('Search'));
	echo(CHtml::endForm());
?>
<hr/>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'enableSorting'=>'true',
	'columns'=>array(
		'uid',
		'nickname',
        'gender',
		'class',
		'camp',

		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
