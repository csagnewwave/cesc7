<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->header="Login";
$this->breadcrumbs=array(
	'Login',
);
Yii::app()->params['registerClose'] = strtotime('2014-04-01 23:59:59');
?>

<?php
if (Yii::app()->user->hasFlash('error')) {
    echo '<div class="error">'.Yii::app()->user->getFlash('error').'</div>';
}
?>
<?php
//$this->widget('ext.eauth.EAuthWidget', array('action' => 'site/login'));
//echo CHtml::link('Login',array('site/login','service'=>'facebook'));//height="42" width="42"
?>
<div class="alert alert-danger" style="text-align: center;">
    <span id="countdown"></span>
</div>
<div class="alert alert-info" style="text-align: center;">
    <strong>ล็อกอินด้วย Facebook เพื่อสมัคร หรือล็อกอิน</strong><br/>
</div>
<div style="text-align: center">
    <a href="index.php?r=site/login&service=facebook#">
        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/facebook.png" alt="Smiley face" style="width:225px">
    </a>
<!--     <div id="fb-root"></div>
    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=1449083385318999";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
    <div class="fb-login-button" data-max-rows="2" data-show-faces="true"></div> -->
</div>
<script type="text/javascript">
        var target_date = <?php echo (Yii::app()->params['registerClose']*1000);/* x1000 for JS*/ ?>;
        //var target_date = new Date("Apr 2, 2014").getTime();
        // variables for time units
        var days, hours, minutes, seconds;
         
        // get tag element
        var countdown = document.getElementById("countdown");
         
        // update the tag with id "countdown" every 1 second
        setInterval(function () {
         
        // find the amount of "seconds" between now and target
        var current_date = new Date().getTime();
        //var current_date = <?php echo time()/1000; ?>;
        var seconds_left = (target_date - current_date) / 1000;

        if(seconds_left>=0){
            // do some time calculations
            days = parseInt(seconds_left / 86400);
            seconds_left = seconds_left % 86400;
             
            hours = parseInt(seconds_left / 3600);
            seconds_left = seconds_left % 3600;
             
            minutes = parseInt(seconds_left / 60);
            seconds = parseInt(seconds_left % 60);
            // format countdown string + set tag value
            countdown.innerHTML = "เหลือเวลาอีก "+days + " วัน, " + hours + " ชั่วโมง "
            + minutes + " นาที " + seconds + " วินาที ";
        }else{
            countdown.innerHTML = "ปิดรับสมัคร ประกาศผลการคัดเลือก วันที่ 10 เมษายน";
        }
         
          
        }, 1000);
    </script>