<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->username,
);

$this->menu=array(
	array('label'=>'List User', 'url'=>array('index')),
	//array('label'=>'Create User', 'url'=>array('create')),
	array('label'=>'Update User', 'url'=>array('update', 'uid'=>$model->username)),
	//array('label'=>'Delete User', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->username),'confirm'=>'Are you sure you want to delete this item?')),
	//array('label'=>'Manage User', 'url'=>array('admin')),
);
?>
<div class="row">
	<div class="col-md-8">
		<h1>View User #<?php echo $model->username; ?></h1>
		<h3><?php echo $model->filestatus?"<p class=\"text-success\">เอกสารครบ &#10004;</p>":"<p class=\"text-danger\">เอกสารไม่ครบ &#x2716;</p>" ?></h3>
		<?php echo CHtml::link("แก้ไขข้อมูล",array('user/update','uid'=>$model->username),array('target'=>'_blank'));?>
	</div>
	<div class="col-md-4">
		<a href="https://www.facebook.com/<?php echo $model->username;  ?>">
			<?php
				$headers = get_headers('http://graph.facebook.com/'.$model->username.'/picture?width=200&height=200',1);
				echo CHtml::image($headers['Location'],"", $htmlOptions=array('class'=>'img-thumbnail') );
			?>
		</a>
	</div>
</div>
<hr/>
เอกสารที่ส่งมาแล้ว (<?php echo CHtml::link("ให้คะแนน",array('user/score','id'=>$model->username),array('target'=>'_blank'));?>)<br/>
<div class="row">
	<div class="col-sm-3">
		ใบสมัคร
	</div>
	<div class="col-sm-3">
		<?php echo CHtml::link("ดู",array('user/pdf','id'=>$model->username),array('target'=>'_blank'))."<br/>"; ?>
	</div>
</div>
<?php
echo "<div class=\"row\">";
echo "<div class=\"col-sm-3\">";
echo CHtml::encode($model->getAttributeLabel('student_pic'));
echo " : ";
if($model->student_pic!=''){
	echo CHtml::link("ดู",array('user/showimage','filename'=>$model->student_pic),array('target'=>'_blank'))."<br/>";
}
echo "</div>";

echo "<div class=\"col-sm-3\">";
echo CHtml::encode($model->getAttributeLabel('other_pic'));
echo " : ";
if($model->other_pic!=''){
	echo CHtml::link("ดู",array('user/showimage','filename'=>$model->other_pic),array('target'=>'_blank'))."<br/>";
}
echo "</div>";

echo "<div class=\"col-sm-3\">";
echo "สำเนาบัตร";
echo " : ";
if($model->sheet_studentid!=''){
	echo CHtml::link("ดู",array('user/showimage','filename'=>$model->sheet_studentid),array('target'=>'_blank'))."<br/>";
}
echo "</div>";

echo "<div class=\"col-sm-3\">";
echo CHtml::encode($model->getAttributeLabel('sheet_parent'));
echo " : ";
if($model->sheet_parent!=''){
	echo CHtml::link("ดู",array('user/showimage','filename'=>$model->sheet_parent),array('target'=>'_blank'))."<br/>";
}
echo "</div>";

echo "<div class=\"col-sm-3\">";
echo CHtml::encode($model->getAttributeLabel('sheet_gen1'));
echo " : ";
if($model->sheet_gen1!=''){
	echo CHtml::link("ดู",array('user/showimage','filename'=>$model->sheet_gen1),array('target'=>'_blank'))."<br/>";
}
echo "</div>";
echo "<div class=\"col-sm-3\">";
echo "หน้า 2 : ";
if($model->sheet_gen2!=''){
	echo CHtml::link("ดู",array('user/showimage','filename'=>$model->sheet_gen2),array('target'=>'_blank'))."<br/>";
}
echo "</div>";
echo "<div class=\"col-sm-3\">";
echo "หน้า 3 : ";
if($model->sheet_gen3!=''){
	echo CHtml::link("ดู",array('user/showimage','filename'=>$model->sheet_gen3),array('target'=>'_blank'))."<br/>";
}
echo "</div>";

echo "</div>";

echo "<div class=\"row\">";
if($model->camp=="Robot"){
	echo "<div class=\"col-sm-3\">";
	echo CHtml::encode($model->getAttributeLabel('sheet_rb1'));
	echo " : ";
	if($model->sheet_rb1!=''){
		echo CHtml::link("ดู",array('user/showimage','filename'=>$model->sheet_rb1),array('target'=>'_blank'))."<br/>";
	}
	echo "</div>";
	echo "<div class=\"col-sm-3\">";
	echo "หน้า 2 : ";
	if($model->sheet_rb2!=''){
		echo CHtml::link("ดู",array('user/showimage','filename'=>$model->sheet_rb2),array('target'=>'_blank'))."<br/>";
	}
	echo "</div>";
	echo "<div class=\"col-sm-3\">";
	echo "หน้า 3 : ";
	if($model->sheet_rb3!=''){
		echo CHtml::link("ดู",array('user/showimage','filename'=>$model->sheet_rb3),array('target'=>'_blank'))."<br/>";
	}
	echo "</div>";
	echo "<div class=\"col-sm-3\">";
	echo "หน้า 4 : ";
	if($model->sheet_rb4!=''){
		echo CHtml::link("ดู",array('user/showimage','filename'=>$model->sheet_rb4),array('target'=>'_blank'))."<br/>";
	}
	echo "</div>";
}else{
	echo "<div class=\"col-sm-3\">";
	echo CHtml::encode($model->getAttributeLabel('sheet_nw1'));
	echo " : ";
	if($model->sheet_nw1!=''){
		echo CHtml::link("ดู",array('user/showimage','filename'=>$model->sheet_nw1),array('target'=>'_blank'))."<br/>";
	}
	echo "</div>";
	echo "<div class=\"col-sm-3\">";
	echo "หน้า 2 : ";
	if($model->sheet_nw2!=''){
		echo CHtml::link("ดู",array('user/showimage','filename'=>$model->sheet_nw2),array('target'=>'_blank'))."<br/>";
	}
	echo "</div>";
	echo "<div class=\"col-sm-3\">";
	echo "หน้า 3 : ";
	if($model->sheet_nw3!=''){
		echo CHtml::link("ดู",array('user/showimage','filename'=>$model->sheet_nw3),array('target'=>'_blank'))."<br/>";
	}
	echo "</div>";
}
echo "</div>";
?>
<hr/>
<div class="panel panel-danger">
	<div class="panel-heading">
		อัพเดตสถานะ
	</div>
	<div class="panel-body">
		<div class="" role="form">

		    <?php $form=$this->beginWidget('CActiveForm', array(
		        'id'=>'user-form',
		        'enableAjaxValidation'=>false,
		    )); ?>

		    <div class="form-group">
		    	<div class="row">
			    	<div class="col-sm-3">
			        <?php   echo $form->labelEx($model,'filestatus',array('class'=>'control-label')); ?>
			        </div>
			        <div class="col-sm-3">
			            <?php echo $form->dropDownList($model,'filestatus', array('0'=>'ยังไม่ได้รับ/เอกสารไม่ครบ','1'=>'ได้รับเอกสาร/เอกสารครบ'),array('class'=>'form-control')); ?>
			        </div>
			    </div>
			    <div class="row">
			    	<div class="col-sm-3">
			        <?php   echo $form->labelEx($model,'byMail',array('class'=>'control-label')); ?>
			        </div>
			        <div class="col-sm-3">
			            <?php echo $form->dropDownList($model,'byMail', array('0'=>'ไม่ได้รับเอกสารทางไปรษณีย์','1'=>'ได้รับเอกสารทางไปรษณีย์'),array('class'=>'form-control')); ?>
			        </div>
			    </div>
			    <div class="row">
			        <div class="col-sm-3">
			        	 <?php   echo $form->labelEx($model,'notice',array('class'=>'control-label')); ?>
			        </div>
			        <div class="col-sm-4">
			            <?php   echo $form->textField($model,'notice',array('class'=>'form-control','size'=>70,'maxlength'=>70,'placeholder'=>'ถ้าเรียบร้อยไม่ต้องใส่อะไร')); ?>
			        </div>		    
		    	</div>
		    	<div class="row">
			        <div class="col-sm-3">
			        	 <?php   echo $form->labelEx($model,'isPrinted',array('class'=>'control-label')); ?>
			        </div>
			        <div class="col-sm-4">
			            <?php echo $form->dropDownList($model,'isPrinted', array('0'=>'ยังไม่ได้พริ้นต์','1'=>'พริ้นต์แล้ว'),array('class'=>'form-control')); ?>
			        </div>		    
		    	</div>
		    	<hr/>
		    	<div class="row">
			        <div class="col-sm-3">
			        	 <?php   echo $form->labelEx($model,'isCesc7',array('class'=>'control-label')); ?>
			        </div>
			        <div class="col-sm-4">
			            <?php echo $form->dropDownList($model,'isCesc7', array('0'=>'NO'),array('class'=>'form-control')); ?>
			        </div>		    
			        <div class="col-sm-3">
			        	<?php
				        	if($_SESSION["isNotSuperuser"]=='false'){
					        	if($model->filestatus)
					        		echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'บันทึก',array('class'=>'btn btn-success form-group')); 
					        	else
					        		echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'บันทึก',array('class'=>'btn btn-danger form-group')); 
					        }
			        	?>
			    	</div>
		    	</div>
		    </div>
		    <?php $this->endWidget(); ?>
		</div>
	</div>
</div>
<hr/>
<h3>ข้อมูล</h3>
<?php

$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'uid' ,
		'surname' ,
		'filestatus',
		'notice',
		'byMail',
				//'username' ,
		'camp',

		'gen1',
		'gen2',
		'gen3',
		'gen4',
		'gen5',
		'gen6',
		'gen7',
		'gen8',
		'rb1',
		'rb2',
		'rb3',
		'rb4',
		'nw1',
		'nw2',
		'nw3',
		'nw4',
		'nw5',
		'nw6',
		'nw7',
		'nw8',

		'email' ,
		'regisTime' ,
		'gender' ,
		'nickname' ,
		'age' ,
		'birthdate' ,
		'address' ,
		'phone_home' ,
		'phone_mobile' ,
		'school' ,
		'branch' ,
		'gpa' ,
		'class' ,
		'region' ,
		'allergy' ,
		'allergy_drug' ,
		'allergy_food' ,
		'camp',
		'regisip',
		'bloodtype',
		'phone_mobile_network',
		'address_province',
		'address_postcode',
		'talent',
		'not_eat',
		'parent_name',
		'parent_relation',
		'parent_phone',
		'school_province',
		'camp1',
		'camp1_uni',
		'camp2',
		'camp2_uni',
		'camp3',
		'camp3_uni',
		'disease',
		'thai_id',
		),
	));

?>
