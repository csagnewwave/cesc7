<?php
    $this->widget('zii.widgets.CListView', array(
        'dataProvider'=>$dataProvider,
        'itemsCssClass'=>'list-view row',
        'itemView'=>'_list',
    ));
?>