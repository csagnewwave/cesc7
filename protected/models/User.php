<?php

/**
 * This is the model class for table "cesc_user".
 *
 * The followings are the available columns in table 'cesc_user':
 * @property integer $uid
 * @property string $username
 * @property string $email
 * @property string $regisTime
 * @property string $gender
 * @property string $surname
 * @property string $nickname
 * @property integer $age
 * @property string $birthdate
 * @property string $address
 * @property string $phone_home
 * @property string $phone_mobile
 * @property string $school
 * @property string $branch
 * @property string $gpa
 * @property string $class
 * @property string $region
 * @property string $allergy
 * @property string $allergy_drug
 * @property string $allergy_food
 * @property string $camp
 * @property string $sheet_parent
 * @property string $sheet_gen1
 * @property string $sheet_gen2
 * @property string $sheet_gen3
 * @property string $sheet_rb1
 * @property string $sheet_rb2
 * @property string $sheet_rb3
 * @property string $sheet_rb4
 * @property string $sheet_nw1
 * @property string $sheet_nw2
 * @property string $sheet_nw3
 * @property string $sheet_nw4
 * @property string $sheet_studentid
 * @property string $regisip
 * @property string $bloodtype
 * @property string $phone_mobile_network
 * @property string $address_province
 * @property string $address_postcode
 * @property string $talent
 * @property string $not_eat
 * @property string $parent_name
 * @property string $parent_relation
 * @property string $parent_phone
 * @property string $school_province
 * @property string $camp1
 * @property string $camp1_uni
 * @property string $camp2
 * @property string $camp2_uni
 * @property string $camp3
 * @property string $camp3_uni
 * @property string $disease
 * @property string $thai_id
 * @property string $student_pic
 * @property string $other_pic
 * @property string $foundby
 * @property integer $filestatus
 * @property integer $istester
 * @property string $notice
 * @property integer $byMail
 * @property integer $isPrinted
 * @property integer $isCesc7
 * @property integer $gen1
 * @property integer $gen2
 * @property integer $gen3
 * @property integer $gen4
 * @property integer $gen5
 * @property integer $gen6
 * @property integer $gen7
 * @property integer $gen8
 * @property integer $rb1
 * @property integer $rb2
 * @property integer $rb3
 * @property integer $rb4
 * @property integer $nw1
 * @property integer $nw2
 * @property integer $nw3
 * @property integer $nw4
 * @property integer $nw5
 * @property integer $nw6
 * @property integer $nw7
 * @property integer $nw8
 *
 */
class User extends CActiveRecord
{



    // holds the password confirmation word
    public $repeatpassword;

    //will hold the encrypted password for update actions.
    public $initialPassword;
    public $verifyCode;//TODO(ziko) : verify code didnt work

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cesc_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('', 'required'),
            //array('username,email, gender, age, camp, phone_mobile, regisip', 'required'),
            //array('gender', 'numerical', 'integerOnly'=>true,'min'=>'0','max'=>'1'),//TODO(ziko):0=girl,1=boy
            array('gender','length', 'max'=>4),//DONE
            array('age', 'numerical', 'integerOnly'=>true,'min'=>'10','max'=>'25'),// limit age
            //array('class', 'numerical', 'integerOnly'=>true,'min'=>'0','max'=>'4'),//TODO(ziko): 1(3>4), 2(4>5), 3(5>6), 4(6>1)
            array('class','length', 'max'=>50),//DONE
            array('camp,sheet_parent,sheet_gen1,sheet_gen2,sheet_gen3, sheet_rb1, sheet_rb2, sheet_rb3, sheet_rb4, sheet_nw1,
            sheet_nw2,sheet_nw3,sheet_studentid','length', 'max'=>7),//DONE
            //array('camp', 'numerical', 'integerOnly'=>true,'min'=>'0','max'=>'1'),//TODO(ziko): 0=robot,1=network
            array('branch, region', 'length', 'max'=>50),

            array('username','length', 'max'=>50),//DONE
            //array('username', 'match', 'pattern'=>'/([a-z0-9_])/'),
            //array('username','unique', 'className' => 'User', 'attributeName' => 'username', 'message'=>'Username นี้ถูกใช้ไปแล้ว'),

            array('email', 'length', 'max'=>150),//DONE
            array('email', 'match', 'pattern'=>'/@.+\./'),
            array('email','unique', 'className' => 'User', 'attributeName' => 'email', 'message'=>'Email นี้ถูกใช้ไปแล้ว'),



//            array('password', 'length','min'=>'4', 'max'=>64),//DONE
            array('school', 'length', 'max'=>150),//DONE

            array('foundby,notice', 'length', 'max'=>250),
            array('filestatus,byMail,isPrinted,isCesc7', 'length', 'max'=>2),
            array('gen1,gen2,gen3,gen4,gen5,gen6,gen7,gen8,rb1,rb2,rb3,rb4,nw1,nw2,nw3,nw4,nw5,nw6,nw7,nw8', 'length', 'max'=>5),

            
            array('surname', 'length', 'max'=>100),//TODO(ziko):need to add regex for correction data
            array('nickname', 'length', 'max'=>20),
            array('surname,nickname,','match', 'pattern'=>'/\D/'),//TODO(ziko):not correctly

            array('bloodtype,phone_mobile_network,address_province,address_postcode,talent,not_eat,parent_name,parent_relation,
            parent_phone,school_province,camp1,camp1_uni,camp2,camp2_uni,camp3,camp3_uni,disease', 'length', 'max'=>200),


            array('address, branch,allergy, allergy_drug, allergy_food', 'length', 'max'=>300),//DONE

            array('thai_id', 'length', 'max'=>17),

            array('gpa', 'length', 'max'=>4),//TODO(ziko):not correctly
            array('gpa','match', 'pattern'=>'/^(0|1|2|3|4)/'),
            //array('gpa','match', 'pattern'=>'/\./'),

            array('phone_home, phone_mobile,parent_phone','match', 'pattern'=>'/^([0-9])/'),//DONE //NOTE:: for number only
            array('phone_mobile','match', 'pattern'=>'/^0+(8|9)/'),
            array('phone_home, phone_mobile', 'length','min'=>'9', 'max'=>10),

            //array('birthdate','match', 'pattern'=>'/^(1|2)+...+\-+..+\-../'),//DONE
            array('birthdate', 'length', 'max'=>11),

//          array('repeatpassword', 'compare', 'compareAttribute'=>'password', 'message'=>"รหัสผ่านไม่ตรงกัน"),//DONE

//            array('verifyCode', 'captcha', 'allowEmpty'=>!Yii::app()->user->isGuest),//DONE


            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            //array('uid, username, password, email, regisTime, gender, surname, nickname, age, birthdate, address, phone_home, phone_mobile, school, branch, gpa, class, region, allergy, allergy_drug, allergy_food, camp', 'safe', 'on'=>'search'),
            array('gpa,regisip,address,birthdate, branch,allergy, allergy_drug, allergy_food, sheet_parent,sheet_gen1,sheet_gen2,sheet_gen3, sheet_rb1, sheet_rb2, sheet_rb3, sheet_rb4, sheet_nw1,
            sheet_nw2,sheet_nw3,sheet_studentid, bloodtype,phone_mobile_network,address_province,address_postcode,talent,not_eat,parent_name,parent_relation,
            parent_phone,school_province,camp1,camp1_uni,camp2,camp2_uni,camp3,camp3_uni,disease,thai_id,other_pic,student_pic,foundby,filestatus,notice,byMail,isPrinted,isCesc7', 'safe','on'=>'edit'),

			array('uid, username, password, email, regisTime, gender, surname, nickname, age, birthdate, address, phone_home, phone_mobile, school, branch, gpa,
			class, region, allergy, allergy_drug, allergy_food, camp, sheet_parent, sheet_rb1, sheet_rb2, sheet_rb3, sheet_rb4, sheet_nw1, sheet_nw2,foundby,
			sheet_nw3, sheet_nw4,filestatus', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'uid' => 'Uid',
			'username' => 'Facebook ID',
			'password' => 'Password',
			'email' => 'Email',
			'regisTime' => 'เวลาลงทะเบียน',
			'gender' => 'เพศ',
			'surname' => 'ชื่อ นามสกุล',
			'nickname' => 'ชื่อเล่น',
			'age' => 'อายุ',
			'birthdate' => 'วันเกิด',
			'address' => 'ที่อยู่',
			'phone_home' => 'เบอร์โทรศัพท์บ้าน',
			'phone_mobile' => 'เบอร์โทรศัพท์มือถือ',
			'school' => 'โรงเรียน',
			'branch' => 'สายวิชา',
			'gpa' => 'เกรดเฉลี่ย',
			'class' => 'ชั้น',
			'region' => 'ศาสนา',
			'allergy' => 'สิ่งที่แพ้',
			'allergy_drug' => 'ยาที่แพ้',
			'allergy_food' => 'อาหารที่แพ้',
			'camp' => 'สาขาที่เลือกอบรม',
			'sheet_parent' => 'ใบขออนุญาตผู้ปกครอง',
			'sheet_gen1' => 'General Quiz หน้า 1',
			'sheet_gen2' => 'General Quiz หน้า 2',
			'sheet_gen3' => 'General Quiz หน้า 3',
			'sheet_rb1' => 'Robot Quiz หน้า 1',
			'sheet_rb2' => 'Robot Quiz หน้า 2',
			'sheet_rb3' => 'Robot Quiz หน้า 3',
			'sheet_rb4' => 'Robot Quiz หน้า 4',
			'sheet_nw1' => 'Network Quiz หน้า 1',
			'sheet_nw2' => 'Network Quiz หน้า 2',
			'sheet_nw3' => 'Network Quiz หน้า 3',
			'sheet_nw4' => 'Network Quiz หน้า 4',
			'sheet_studentid' => 'สำเนาบัตรนักเรียน/ บัตรประชาชน',
            'verifyCode'=>'รหัสยืนยัน',
            'regisip'=>'Register IP',
            'bloodtype'=>'กรุ๊ปเลือด',
            'phone_mobile_network'=>'เครือข่ายโทรศัพท์มือถือ',
            'address_province'=>'จังหวัด',
            'address_postcode'=>'รหัสไปรษณีย์',
            'talent'=>'ความสามารถพิเศษ',
            'not_eat'=>'ไม่รับประทาน',
            'parent_name'=>'ชื่อ นามสกุล ผู้ปกครอง',
            'parent_relation'=>'ความเกี่ยวข้อง',
            'parent_phone'=>'เบอร์โทรศัพท์',
            'school_province'=>'จังหวัด(โรงเรียน)',
            'camp1'=>'1.ค่าย',
            'camp1_uni'=>'  จัดโดย',
            'camp2'=>'2.ค่าย',
            'camp2_uni'=>'  จัดโดย',
            'camp3'=>'3.ค่าย',
            'camp3_uni'=>'  จัดโดย',
            'disease'=>'โรคประจำตัว',
            'thai_id'=>'เลขประจำตัวประชาชน',
            'other_pic'=>'รูปถ่ายไม่เป็นทางการ',
            'filestatus'=>'สถานะของเอกสาร',
            'student_pic'=>'รูปนักเรียน 2 นิ้ว',
            'foundby'=>'รู้จักค่าย CE Smart Camp7 จากช่องทาง',
            'notice'=>'แจ้งเตือน',
            'byMail'=>'ส่งเอกสารทางไปรษณีย์',
            'isPrinted'=>'พริ้นต์แล้ว',
            'isCesc7'=>'เป็นน้องค่าย CESC#7',
            'gen1'=>'คะแนน Gen1',
            'gen2'=>'คะแนน Gen2',
            'gen3'=>'คะแนน Gen3',
            'gen4'=>'คะแนน Gen4',
            'gen5'=>'คะแนน Gen5',
            'gen6'=>'คะแนน Gen6',
            'gen7'=>'คะแนน Gen7',
            'gen8'=>'คะแนน Gen8',
            'rb1'=>'คะแนน rb1',
            'rb2'=>'คะแนน rb2',
            'rb3'=>'คะแนน rb3',
            'rb4'=>'คะแนน rb4',
            'nw1'=>'คะแนน nw1',
            'nw2'=>'คะแนน nw2',
            'nw3'=>'คะแนน nw3',
            'nw4'=>'คะแนน nw4',
            'nw5'=>'คะแนน nw5',
            'nw6'=>'คะแนน nw6',
            'nw7'=>'คะแนน nw7',
            'nw8'=>'คะแนน nw8');
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->compare('uid',$this->uid);
		$criteria->compare('username',$this->username,true);
		//$criteria->compare('email',$this->email,true);
		//$criteria->compare('regisTime',$this->regisTime,true);
		$criteria->compare('gender',$this->gender);
		$criteria->compare('surname',$this->surname,true);
		$criteria->compare('nickname',$this->nickname,true);
		/*$criteria->compare('age',$this->age);
		$criteria->compare('birthdate',$this->birthdate,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('phone_home',$this->phone_home,true);
		$criteria->compare('phone_mobile',$this->phone_mobile,true);
		$criteria->compare('class',$this->class);
		$criteria->compare('region',$this->region,true);
		$criteria->compare('allergy',$this->allergy,true);
		$criteria->compare('allergy_drug',$this->allergy_drug,true);
		$criteria->compare('allergy_food',$this->allergy_food,true);*/
		$criteria->compare('camp',$this->camp);
		$criteria->condition = 'istester=:istester';
		$criteria->params = array(':istester'=>0);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,

			'sort'=>array(
		        'defaultOrder'=>'uid DESC',
		    ),
		    'pagination'=>array(
		        'pageSize'=>'200'
		    ),

		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    //ziko: for Authentication User
    public function validatePassword($password)
    {
        /*echo "<script> console.log('".$this->username."');</script>";// @ziko : for testing
        echo "<script> console.log('".$this->password."');</script>";
        echo "<script> console.log('".$this->hashPassword($password,$this->username)."');</script>";*/
        //return $this->hashPassword($password,$this->username)===$this->password;
        return $password===$this->password;
    }

    public function hashPassword($password,$username)
    {
        return sha1($username.$password);
    }

}
