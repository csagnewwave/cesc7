<?php
/* @var $this UserController */
/* @var $model User */
$this->setPageTitle(Yii::app()->name .' - สมัครเข้าค่าย');
$this->breadcrumbs=array(
    'Users'=>array('index'),
    'Create',
);

$this->menu=array();
?>
<div class="alert alert-danger" style="text-align: center;">
    <span id="countdown"></span>
</div>';

<?php
	$regisCloseTimestamp = Yii::app()->params['registerClose'];
	//$regisCloseTimestamp = strtotime('2014-04-01 23:59:59');
    $currentDateTimestamp = time();

    if ($regisCloseTimestamp  < $currentDateTimestamp) {

    }else{
		$this->renderPartial('_signup', array('model'=>$model));
	}
?>


<script type="text/javascript">
		var target_date = <?php echo (Yii::app()->params['registerClose']*1000);/* x1000 for JS*/ ?>;
        //var target_date = new Date("Apr 2, 2014").getTime();
        // variables for time units
        var days, hours, minutes, seconds;
         
        // get tag element
        var countdown = document.getElementById("countdown");
         
        // update the tag with id "countdown" every 1 second
        setInterval(function () {
         
        // find the amount of "seconds" between now and target
        var current_date = new Date().getTime();
        //var current_date = <?php echo time()/1000; ?>;
        var seconds_left = (target_date - current_date) / 1000;

        if(seconds_left>=0){
            // do some time calculations
            days = parseInt(seconds_left / 86400);
            seconds_left = seconds_left % 86400;
             
            hours = parseInt(seconds_left / 3600);
            seconds_left = seconds_left % 3600;
             
            minutes = parseInt(seconds_left / 60);
            seconds = parseInt(seconds_left % 60);
            // format countdown string + set tag value
            countdown.innerHTML = "เหลือเวลาอีก "+days + " วัน, " + hours + " ชั่วโมง "
            + minutes + " นาที " + seconds + " วินาที ";
        }else{
            countdown.innerHTML = "ปิดรับสมัคร ประกาศผลการคัดเลือก วันที่ 10 เมษายน";
        }
         
          
        }, 1000);
    </script>