<div class="" role="form">
		<?php $form=$this->beginWidget('CActiveForm',
		array(
		'id'=>'user-form',
		'enableAjaxValidation'=>false,
		));
		?>

		<div class="form-group">
		<div class="row">
			<div class="col-sm-3">
			</div>
		</div>

		<?php
			echo "<div class=\"row\">";
			echo "<div class=\"col-sm-12\">";
			echo CHtml::encode($model->getAttributeLabel('sheet_gen1'));
			if($model->sheet_gen1!=''){
				echo '<img style="max-width:100%;" src="' . $this->createUrl('/user/showimage', array('filename'=>$model->sheet_gen1)) . '" />';
			}
			echo "</div>";

			echo '<div class="col-sm-3">';
			echo $form->labelEx($model,'gen1',array('class'=>'control-label'));
		    echo $form->textField($model,'gen1',array('class'=>'form-control','size'=>70,'maxlength'=>70,'placeholder'=>'Integer'));
		    echo "</div>";
		    echo '<div class="col-sm-3">';
			echo $form->labelEx($model,'gen2',array('class'=>'control-label'));
		    echo $form->textField($model,'gen2',array('class'=>'form-control','size'=>70,'maxlength'=>70,'placeholder'=>'Integer'));
		    echo "</div>";
		    echo '<div class="col-sm-3">';
			echo $form->labelEx($model,'gen3',array('class'=>'control-label'));
		    echo $form->textField($model,'gen3',array('class'=>'form-control','size'=>70,'maxlength'=>70,'placeholder'=>'Integer'));
		    echo "</div>";
		    echo '<div class="col-sm-3">';
			echo $form->labelEx($model,'gen4',array('class'=>'control-label'));
		    echo $form->textField($model,'gen4',array('class'=>'form-control','size'=>70,'maxlength'=>70,'placeholder'=>'Integer'));
		    echo "</div>";
		    echo "</div>";
		    echo "<hr/>";

			echo "<div class=\"row\">";
			echo "<div class=\"col-sm-12\">";
			echo CHtml::encode($model->getAttributeLabel('sheet_gen2'));
			if($model->sheet_gen2!=''){
				echo '<img style="max-width:100%;" src="' . $this->createUrl('/user/showimage', array('filename'=>$model->sheet_gen2)) . '" />';
			}
			echo "</div>";
			echo '<div class="col-sm-3">';
			echo $form->labelEx($model,'gen5',array('class'=>'control-label'));
		    echo $form->textField($model,'gen5',array('class'=>'form-control','size'=>70,'maxlength'=>70,'placeholder'=>'Integer'));
		    echo "</div>";
		    echo '<div class="col-sm-3">';
			echo $form->labelEx($model,'gen6',array('class'=>'control-label'));
		    echo $form->textField($model,'gen6',array('class'=>'form-control','size'=>70,'maxlength'=>70,'placeholder'=>'Integer'));
		    echo "</div>";
		    echo "</div>";
		    echo "<hr/>";

		    echo "<div class=\"row\">";
			echo "<div class=\"col-sm-12\">";
			echo CHtml::encode($model->getAttributeLabel('sheet_gen3'));
			if($model->sheet_gen3!=''){
				echo '<img style="max-width:100%;" src="' . $this->createUrl('/user/showimage', array('filename'=>$model->sheet_gen3)) . '" />';
			}
			echo "</div>";
			echo '<div class="col-sm-3">';
			echo $form->labelEx($model,'gen7',array('class'=>'control-label'));
		    echo $form->textField($model,'gen7',array('class'=>'form-control','size'=>70,'maxlength'=>70,'placeholder'=>'Integer'));
		    echo "</div>";
		    echo '<div class="col-sm-3">';
			echo $form->labelEx($model,'gen8',array('class'=>'control-label'));
		    echo $form->textField($model,'gen8',array('class'=>'form-control','size'=>70,'maxlength'=>70,'placeholder'=>'Integer'));
		    echo "</div>";
			echo "</div>";
			echo "<hr/>";



			if($model->camp=="Robot"){
				echo '<div class="row">';
				echo "<div class=\"col-sm-12\">";
				echo CHtml::encode($model->getAttributeLabel('sheet_rb1'));
				if($model->sheet_rb1!=''){
					echo '<img style="max-width:100%;" src="' . $this->createUrl('/user/showimage', array('filename'=>$model->sheet_rb1)) . '" />';
				}
				echo "</div>";
				echo '<div class="col-sm-3">';
				echo $form->labelEx($model,'rb1',array('class'=>'control-label'));
			    echo $form->textField($model,'rb1',array('class'=>'form-control','size'=>70,'maxlength'=>70,'placeholder'=>'Integer'));
			    echo "</div>";			    
			    echo "</div>";
			    echo "<hr/>";


			    echo '<div class="row">';
				echo "<div class=\"col-sm-12\">";
				echo $form->labelEx($model,'rb2',array('class'=>'control-label'));
				if($model->sheet_rb4!=''){
					echo '<img style="max-width:100%;" src="' . $this->createUrl('/user/showimage', array('filename'=>$model->sheet_rb2)) . '" />';
				}
				echo "</div>";
			    echo '<div class="col-sm-3">';
				echo $form->labelEx($model,'rb2',array('class'=>'control-label'));
			    echo $form->textField($model,'rb2',array('class'=>'form-control','size'=>70,'maxlength'=>70,'placeholder'=>'Integer'));
			    echo "</div>";
			    echo "</div>";
			    echo "<hr/>";


			    echo '<div class="row">';
				echo "<div class=\"col-sm-12\">";
				echo $form->labelEx($model,'rb3',array('class'=>'control-label'));
				if($model->sheet_rb3!=''){
					echo '<img style="max-width:100%;" src="' . $this->createUrl('/user/showimage', array('filename'=>$model->sheet_rb3)) . '" />';
				}
				echo "</div>";
				echo '<div class="col-sm-3">';
				echo $form->labelEx($model,'rb3',array('class'=>'control-label'));
			    echo $form->textField($model,'rb3',array('class'=>'form-control','size'=>70,'maxlength'=>70,'placeholder'=>'Integer'));
			    echo "</div>";
				echo "</div>";
				echo "<hr/>";

				echo '<div class="row">';
				echo "<div class=\"col-sm-12\">";
				echo $form->labelEx($model,'rb4',array('class'=>'control-label'));
				if($model->sheet_rb4!=''){
					echo '<img style="max-width:100%;" src="' . $this->createUrl('/user/showimage', array('filename'=>$model->sheet_rb4)) . '" />';
				}
				echo "</div>";
				echo '<div class="col-sm-3">';
				echo $form->labelEx($model,'rb4',array('class'=>'control-label'));
			    echo $form->textField($model,'rb4',array('class'=>'form-control','size'=>70,'maxlength'=>70,'placeholder'=>'Integer'));
			    echo "</div>";
				echo "</div>";
				echo "<hr/>";
			}else{
				echo '<div class="row">';
				echo "<div class=\"col-sm-12\">";
				echo CHtml::encode($model->getAttributeLabel('sheet_nw1'));
				if($model->sheet_nw1!=''){
					echo '<img style="max-width:100%;" src="' . $this->createUrl('/user/showimage', array('filename'=>$model->sheet_nw1)) . '" />';
				}
				echo "</div>";
				echo '<div class="col-sm-3">';
				echo $form->labelEx($model,'nw1',array('class'=>'control-label'));
			    echo $form->textField($model,'nw1',array('class'=>'form-control','size'=>70,'maxlength'=>70,'placeholder'=>'Integer'));
			    echo "</div>";
			    echo '<div class="col-sm-3">';
				echo $form->labelEx($model,'nw2',array('class'=>'control-label'));
			    echo $form->textField($model,'nw2',array('class'=>'form-control','size'=>70,'maxlength'=>70,'placeholder'=>'Integer'));
			    echo "</div>";
			    echo '<div class="col-sm-3">';
				echo $form->labelEx($model,'nw3',array('class'=>'control-label'));
			    echo $form->textField($model,'nw3',array('class'=>'form-control','size'=>70,'maxlength'=>70,'placeholder'=>'Integer'));
			    echo "</div>";
			    echo '<div class="col-sm-3">';
				echo $form->labelEx($model,'nw4',array('class'=>'control-label'));
			    echo $form->textField($model,'nw4',array('class'=>'form-control','size'=>70,'maxlength'=>70,'placeholder'=>'Integer'));
			    echo "</div>";
				echo "</div>";
				echo "<hr/>";

				echo '<div class="row">';
				echo "<div class=\"col-sm-12\">";
				echo CHtml::encode($model->getAttributeLabel('sheet_nw2'));
				if($model->sheet_nw2!=''){
					echo '<img style="max-width:100%;" src="' . $this->createUrl('/user/showimage', array('filename'=>$model->sheet_nw2)) . '" />';
				}
				echo "</div>";
				echo '<div class="col-sm-3">';
				echo $form->labelEx($model,'nw5',array('class'=>'control-label'));
			    echo $form->textField($model,'nw5',array('class'=>'form-control','size'=>70,'maxlength'=>70,'placeholder'=>'Integer'));
			    echo "</div>";
			    echo '<div class="col-sm-3">';
				echo $form->labelEx($model,'nw6',array('class'=>'control-label'));
			    echo $form->textField($model,'nw6',array('class'=>'form-control','size'=>70,'maxlength'=>70,'placeholder'=>'Integer'));
			    echo "</div>";
				echo "</div>";
				echo "<hr/>";

				echo '<div class="row">';
				echo "<div class=\"col-sm-12\">";
				echo CHtml::encode($model->getAttributeLabel('sheet_nw3'));
				if($model->sheet_nw3!=''){
					echo '<img style="max-width:100%;" src="' . $this->createUrl('/user/showimage', array('filename'=>$model->sheet_nw3)) . '" />';
				}
				echo "</div>";
				echo '<div class="col-sm-3">';
				echo $form->labelEx($model,'nw7',array('class'=>'control-label'));
			    echo $form->textField($model,'nw7',array('class'=>'form-control','size'=>70,'maxlength'=>70,'placeholder'=>'Integer'));
			    echo "</div>";
			    echo '<div class="col-sm-3">';
				echo $form->labelEx($model,'nw8',array('class'=>'control-label'));
			    echo $form->textField($model,'nw8',array('class'=>'form-control','size'=>70,'maxlength'=>70,'placeholder'=>'Integer'));
			    echo "</div>";
				echo "</div>";
				echo "<hr/>";
			}
			echo "</div>";

			echo CHtml::submitButton('Save',array('class'=>'btn btn-primary form-group')); 
		?>
	</div>
	<?php $this->endWidget(); ?>
</div>