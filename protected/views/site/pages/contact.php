﻿<?php
/* @var $this SiteController */
$this->layout='column1';
$this->pageTitle=Yii::app()->name . ' - ติดต่อ';
$this->breadcrumbs=array(
	'Contact',
);
?>
<div class="row">
	<div class="col-md-6 col-sm-6">
		<iframe  width = "70%" src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fcesmartcamp&amp;width&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=false&amp;appId=529715883763582" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:258px;" allowTransparency="true"></iframe>
	</div>
</div>

<div class="row">
	<div class="col-md-6 col-sm-6">
		<h2>ติดต่อ</h2>
		<div class="alert alert-info">
			พี่เตย : 0885838135<br/>
			พี่โก้ : 0876746379<br/>
			พี่ปอย : 0863254935<br/>
		</div>
	</div>
	<div class="col-md-6 col-sm-6">
		<h2>ที่อยู่</h2>
		<div class="alert alert-info">
			<address>
			<strong>ภาควิชาวิศวกรรมคอมพิวเตอร์ (CE SMART Camp)</strong><br/>
			คณะวิศวกรรมศาสตร์ สถาบันเทคโนโลยีพระจอมเกล้าเจ้าคุณทหารลาดกระบังชั้น 9, อาคารปฏิบัติการรวมวิศวกรรมศาสตร์ 2 (ECC) เลขที่ 1 ซอยฉลองกรุง 1 แขวงลาดกระบัง, เขตลาดกระบัง, กรุงเทพ 10520<br/>
			(วงเล็บมุมซองว่า "CE Smart Camp #7")
			</address>
		</div>
	</div>
</div>
