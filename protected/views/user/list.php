<?php
/* @var $this UploadController */
/* @var $dataProvider CActiveDataProvider */

?>

<h1>รายชื่อน้องที่ส่งเอกสารครบแล้ว</h1>
<?php
    $count1 = User::Model()->count("filestatus=1 AND camp='Robot' AND gender='ชาย'");
    $count2 = User::Model()->count("filestatus=1 AND camp='Robot' AND gender<>'ชาย'");
    echo '<hr/>';
    echo '<h2>ROBOT ช.'.$count1.' ญ.'.$count2.'</h2>';
    echo '<div class="alert alert-success">';
    echo "<h3>พริ้นต์แล้ว</h3>";
	$criteria=new CDbCriteria();
    //Boy
    echo "ชาย";
    $criteria->condition = "istester=0 AND filestatus=1 AND camp='Robot' AND isPrinted='1' AND gender='ชาย'";
    $dataProvider=new CActiveDataProvider('User', array(
        'criteria'=>$criteria,
        'sort'=>array(
            'defaultOrder'=>'surname ASC',
            ),
        'pagination'=>array(
            'pageSize'=>'100'
            ),
        ));
	$this->widget('zii.widgets.CListView', array(
		'dataProvider'=>$dataProvider,
		'itemsCssClass'=>'list-view row',
		'itemView'=>'_list',
	));
    //Girl
    echo "หญิง";
    $criteria->condition = "istester=0 AND filestatus=1 AND camp='Robot' AND isPrinted='1' AND gender<>'ชาย'";
    $dataProvider=new CActiveDataProvider('User', array(
        'criteria'=>$criteria,
        'sort'=>array(
            'defaultOrder'=>'surname ASC',
            ),
        'pagination'=>array(
            'pageSize'=>'100'
            ),
        ));
    $this->widget('zii.widgets.CListView', array(
        'dataProvider'=>$dataProvider,
        'itemsCssClass'=>'list-view row',
        'itemView'=>'_list',
    ));
    echo '</div>';

    echo '<div class="alert alert-warning">';
    echo "<h3>ยังไม่ได้พริ้นต์</h3>";
    //Boy
    echo "ชาย";
    $criteria->condition = "istester=0 AND filestatus=1 AND camp='Robot' AND isPrinted<>'1' AND gender='ชาย'";
    $dataProvider=new CActiveDataProvider('User', array(
        'criteria'=>$criteria,
        'sort'=>array(
            'defaultOrder'=>'surname ASC',
            ),
        'pagination'=>array(
            'pageSize'=>'100'
            ),
        ));

	$this->widget('zii.widgets.CListView', array(
		'dataProvider'=>$dataProvider,
		'itemsCssClass'=>'list-view row',
		'itemView'=>'_list',
	));
    //Girl
    echo "หญิง";
    $criteria->condition = "istester=0 AND filestatus=1 AND camp='Robot' AND isPrinted<>'1' AND gender<>'ชาย'";
    $dataProvider=new CActiveDataProvider('User', array(
        'criteria'=>$criteria,
        'sort'=>array(
            'defaultOrder'=>'surname ASC',
            ),
        'pagination'=>array(
            'pageSize'=>'100'
            ),
        ));
    $this->widget('zii.widgets.CListView', array(
        'dataProvider'=>$dataProvider,
        'itemsCssClass'=>'list-view row',
        'itemView'=>'_list',
    ));
    echo '</div>';
?>
<br/>
<hr/>
<?php
    $count1 = User::Model()->count("filestatus=1 AND camp<>'Robot' AND gender='ชาย'");
    $count2 = User::Model()->count("filestatus=1 AND camp<>'Robot' AND gender<>'ชาย'");
    echo '<h2>NETWORK ช.'.$count1.' ญ.'.$count2.'</h2>';
    echo '<div class="alert alert-success">';
	echo "<h3>พริ้นต์แล้ว</h3>";
	$criteria=new CDbCriteria();
    echo "ชาย";
    $criteria->condition = "istester=0 AND filestatus=1 AND camp<>'Robot' AND isPrinted='1' AND gender='ชาย'";
    $dataProvider=new CActiveDataProvider('User', array(
        'criteria'=>$criteria,
        'sort'=>array(
            'defaultOrder'=>'surname ASC',
            ),
        'pagination'=>array(
            'pageSize'=>'100'
            ),

        ));

	$this->widget('zii.widgets.CListView', array(
		'dataProvider'=>$dataProvider,
		'itemsCssClass'=>'list-view row',
		'itemView'=>'_list',
	));
    //Girl
    echo "หญิง";
    $criteria->condition = "istester=0 AND filestatus=1 AND camp<>'Robot' AND isPrinted='1' AND gender<>'ชาย'";
    $dataProvider=new CActiveDataProvider('User', array(
        'criteria'=>$criteria,
        'sort'=>array(
            'defaultOrder'=>'surname ASC',
            ),
        'pagination'=>array(
            'pageSize'=>'100'
            ),

        ));

    $this->widget('zii.widgets.CListView', array(
        'dataProvider'=>$dataProvider,
        'itemsCssClass'=>'list-view row',
        'itemView'=>'_list',
    ));
    echo '</div>';

    echo '<div class="alert alert-warning">';
	echo "<h3>ยังไม่ได้พริ้นต์</h3>";
    //Boy
    echo "ชาย";
    $criteria->condition = "istester=0 AND filestatus=1 AND camp<>'Robot' AND isPrinted<>'1' AND gender='ชาย'";
    $dataProvider=new CActiveDataProvider('User', array(
        'criteria'=>$criteria,
        'sort'=>array(
            'defaultOrder'=>'surname ASC',
            ),
        'pagination'=>array(
            'pageSize'=>'100'
            ),

        ));

	$this->widget('zii.widgets.CListView', array(
		'dataProvider'=>$dataProvider,
		'itemsCssClass'=>'list-view row',
		'itemView'=>'_list',
	));
    //Girl
    echo "หญิง";
    $criteria->condition = "istester=0 AND filestatus=1 AND camp<>'Robot' AND isPrinted<>'1' AND gender<>'ชาย'";
    $dataProvider=new CActiveDataProvider('User', array(
        'criteria'=>$criteria,
        'sort'=>array(
            'defaultOrder'=>'surname ASC',
            ),
        'pagination'=>array(
            'pageSize'=>'100'
            ),

        ));

    $this->widget('zii.widgets.CListView', array(
        'dataProvider'=>$dataProvider,
        'itemsCssClass'=>'list-view row',
        'itemView'=>'_list',
    ));
    echo '</div>';
?>