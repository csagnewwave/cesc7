<?php
/* @var $this UploadController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Photo',
);

?>

<h1>Uploads</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
